# javascript-colour-changer
This allows you to change the colour of the background of a website page or item,
You can also use a pad to change the colour and select from which hexadecimal colours.

I've also done a separate version with a pad, checkout pad-index.html for that version.
###<a href="https://ruddernation-designs.github.io/colour-changer" target="_blank" title="Colour Changer Demo">Demo</a>
###<a href="https://ruddernation-designs.github.io/colour-changer/pad" target="_blank" title="Pad Demo">Pad Demo</a>
